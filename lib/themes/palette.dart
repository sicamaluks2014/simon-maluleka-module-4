import 'package:flutter/material.dart';

class Palette {
  static const MaterialColor kToDark = const MaterialColor(
    0xff889637, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    const <int, Color>{
      50: const Color(0xff7a8732), //10%
      100: const Color(0xff6d782c), //20%
      200: const Color(0xff5f6927), //30%
      300: const Color(0xff525a21), //40%
      400: const Color(0xff444b1c), //50%
      500: const Color(0xff363c16), //60%
      600: const Color(0xff292d10), //70%
      700: const Color(0xff1b1e0b), //80%
      800: const Color(0xff0e0f05), //90%
      900: const Color(0xff000000), //100%
    },
  );
}
