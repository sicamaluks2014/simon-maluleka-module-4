import 'package:flutter/material.dart';
import 'package:m3_assessment_1/screens/login.dart';
import 'package:m3_assessment_1/screens/privacy.dart';
import 'package:m3_assessment_1/screens/terms.dart';
// import 'package:url_launcher/url_launcher.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool checked = false;
  final Uri termsUrl = Uri.parse("https://");
  final Uri privacyUrl = Uri.parse("https://");

  bool _toggleChecked() {
    return checked = !checked;
  }

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
  }

  // void _launchUrl(Uri url) async {
  //   if (!await launchUrl(url)) throw 'Could not launch $url';
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xff889637),
      body: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
            height: 150,
            width: MediaQuery.of(context).size.width,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(75.0),
                bottomRight: Radius.circular(75.0),
              ),
              color: Color(0xfff7f7f7),
            ),
            child: const Center(
              child: Image(
                image: AssetImage('assets/naildeck.jpeg'),
              ),
            ),
          ),
          const SizedBox(
            height: 20.0,
          ),
          Center(
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(75.0),
                  topRight: Radius.circular(75.0),
                ),
                color: Color(0xfff7f7f7),
              ),
              height: 632,
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Get Started',
                    style: TextStyle(
                        color: Color(0xff889637),
                        letterSpacing: 5.0,
                        fontFamily: 'roboto',
                        fontWeight: FontWeight.bold,
                        fontSize: 34),
                  ),
                  const SizedBox(
                    height: 80,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      children: [
                        TextFormField(
                          decoration: const InputDecoration(
                            hintText: 'Name',
                          ),
                        ),
                        TextFormField(
                          decoration: const InputDecoration(
                            hintText: 'E-mail',
                          ),
                        ),
                        TextFormField(
                          decoration: const InputDecoration(
                            hintText: 'Password',
                          ),
                          obscureText: true,
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: [
                      Checkbox(
                          value: checked,
                          activeColor: const Color(0xff889637),
                          onChanged: (v) {
                            setState(() {
                              _toggleChecked();
                            });
                          }),
                      const Text("I agree to the "),
                      InkWell(
                        onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => const TermsOfUse())),
                        child: RichText(
                          text: const TextSpan(
                            text: "Terms of Service",
                            style: TextStyle(
                              color: Color(0xff889637),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      const Text(" and "),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => const Privacy()));
                        },
                        child: RichText(
                          text: const TextSpan(
                            text: "Privacy Policy",
                            style: TextStyle(
                                color: Color(0xff889637),
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Sign Up",
                          style: TextStyle(
                            fontFamily: "Roboto",
                            fontSize: 30.0,
                            color: Color(0xff889637),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        FloatingActionButton.large(
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) => const Login()),
                            );
                          },
                          foregroundColor: Color(0xfff7f7f7),
                          elevation: 10.0,
                          tooltip: "Sign-up",
                          backgroundColor: const Color(0xff889637),
                          child: const Icon(Icons.arrow_forward),
                        ),
                      ],
                    ),
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: InkWell(
                        onTap: (() => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => const Login()))),
                        child: RichText(
                          text: const TextSpan(
                            text: "Sign In",
                            style: TextStyle(
                                color: Color(0xff889637),
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ]),
                  const SizedBox(height: 80.0),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Text(
                          "App developed by ",
                          style: TextStyle(
                            fontFamily: "roboto",
                            color: Colors.lightBlue,
                          ),
                        ),
                        Text(
                          "Simon Maluleka",
                          style: TextStyle(
                              fontFamily: "roboto",
                              fontSize: 16,
                              color: Color(0xff889637),
                              fontWeight: FontWeight.bold),
                        ),
                      ])
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
