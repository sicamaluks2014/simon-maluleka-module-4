import 'package:flutter/material.dart';

class Chat extends StatefulWidget {
  Chat({Key? key}) : super(key: key);

  @override
  State<Chat> createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("The Nail Deck Chatbot"),
      ),
      body: Container(
        child: Column(
          children: [
            Flexible(
                child: ListView(
              children: [],
            )),
            const Divider(
              height: 3.0,
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              margin: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(children: [
                const Flexible(
                  child: TextField(
                    decoration: InputDecoration.collapsed(
                        hintText: "Send your message"),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 4.0),
                  child: IconButton(
                      onPressed: () {}, icon: const Icon(Icons.send)),
                )
              ]),
            )
          ],
        ),
      ),
    );
  }
}
