import 'dart:io';

import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:image_picker/image_picker.dart';
import 'package:m3_assessment_1/themes/palette.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  PickedFile? _imageFile;
  final ImagePicker _picker = ImagePicker();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xff889637),
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Edit User Profile",
          style: TextStyle(fontFamily: "roboto"),
        ),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 42,
          ),
          Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(75.0),
                topRight: Radius.circular(75.0),
              ),
              color: Color(0xfff7f7f7),
            ),
            height: MediaQuery.of(context).size.height - 100,
            width: MediaQuery.of(context).size.width,
            child: ListView(
              children: <Widget>[
                const SizedBox(height: 20),
                imageProfile(),
                const SizedBox(height: 20),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: formTextField(
                      'Name',
                      TextInputType.name,
                      const Icon(
                        Icons.person,
                        color: Palette.kToDark,
                      )),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: formTextField(
                      'Profession',
                      TextInputType.text,
                      const Icon(
                        Icons.work,
                        color: Palette.kToDark,
                      )),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: formTextField(
                      'Date of birth',
                      TextInputType.text,
                      const Icon(
                        Icons.calendar_month_rounded,
                        color: Palette.kToDark,
                      )),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: formTextField(
                      'Title',
                      TextInputType.text,
                      const Icon(
                        Icons.title,
                        color: Palette.kToDark,
                      )),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: formTextField(
                      'About',
                      TextInputType.text,
                      const Icon(
                        Icons.info,
                        color: Palette.kToDark,
                      )),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: ElevatedButton(
                    onPressed: () {},
                    child: const Text(
                      "Save",
                      style: TextStyle(fontFamily: "boroto"),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget formTextField(String label, TextInputType keyboardType, Icon icon) {
    return TextFormField(
        keyboardType: keyboardType,
        decoration: InputDecoration(
          border: const OutlineInputBorder(
              borderSide: BorderSide(color: Palette.kToDark)),
          focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 2)),
          prefixIcon: icon,
          labelText: label,
          hintText: 'Enter your $label',
          helperText: '$label is required',
        ));
  }

  Widget imageProfile() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Stack(
          children: <Widget>[
            CircleAvatar(
              radius: 80.0,
              backgroundImage:
                  _imageFile == null ? null : FileImage(File(_imageFile!.path)),
            ),
            Positioned(
              bottom: 20,
              right: 30,
              child: InkWell(
                onTap: () => showModalBottomSheet(
                  context: context,
                  builder: ((builder) => bottomSheet()),
                ),
                child: const Icon(
                  Icons.camera_alt,
                  color: Palette.kToDark,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget bottomSheet() {
    return Container(
      height: 100.0,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Center(
        child: Column(children: <Widget>[
          const Text(
            'Choose a Profile Photo',
            style: TextStyle(fontSize: 20.0),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextButton.icon(
                onPressed: () {
                  takePhoto(ImageSource.camera);
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.camera),
                label: const Text('Camera'),
              ),
              TextButton.icon(
                  onPressed: () {
                    takePhoto(ImageSource.gallery);
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.image),
                  label: const Text('Gallery')),
            ],
          )
        ]),
      ),
    );
  }

  void takePhoto(ImageSource source) async {
    final pickedFile = await _picker.pickImage(source: source);
    setState(() {
      _imageFile = pickedFile as PickedFile?;
    });
  }
}
