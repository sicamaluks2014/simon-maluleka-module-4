import 'package:flutter/material.dart';

class TermsOfUse extends StatelessWidget {
  const TermsOfUse({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lime,
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 15.0, left: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(Icons.arrow_back_ios),
                  color: const Color.fromARGB(255, 4, 55, 85),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const Text(
                      "Terms Of Use",
                      style: TextStyle(
                          fontFamily: "roboto",
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 4, 55, 85)),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.menu),
                      color: const Color.fromARGB(255, 4, 55, 85),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 40.0,
          ),
          Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(75.0),
                topRight: Radius.circular(75.0),
              ),
              color: Color(0xfff7f7f7),
            ),
            padding: const EdgeInsets.only(
                top: 20.0, left: 20.0, right: 20.0, bottom: 10.0),
            height: 700,
            child: ListView(
              children: const [
                Center(
                  child: Text(
                    "Terms and Conditions",
                    style: TextStyle(
                      fontFamily: "roboto",
                      fontSize: 30,
                      color: Color(0xff889637),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Text(
                  "Last updated: May 18, 2022",
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 16,
                    color: Color(0xff889637),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  "Please read these terms and conditions carefully before using our service.",
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 16,
                    color: Color(0xff889637),
                  ),
                ),
                SizedBox(height: 30),
                Text(
                  "Interpretation and Definitions",
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 20,
                    color: Color(0xff889637),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 30),
                Text(
                  "Interpretation",
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 20,
                    color: Color(0xff889637),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  "The words of which the initial letter is capitalized have meanings defined under the following conditions. The following definitions shall have the same meaning regardless of whether they appear in singular or plural.",
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 16,
                    color: Color(0xff889637),
                  ),
                ),
                SizedBox(height: 30),
                Text(
                  "Definitions",
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 20,
                    color: Color(0xff889637),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  "For the puprose of these Terms and Conditions.",
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 16,
                    color: Color(0xff889637),
                  ),
                ),
                SizedBox(height: 30),
                Text(
                  '1. Affiliate means an entity that controls, is controlled by, or is under common control with a party, where "control" means 50% or more of the shares, equity interest or other securities entitled to vote for election of directors or other managing authority.',
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 16,
                    color: Color(0xff889637),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  '2. Account means a unique account created for You to access our Service or parts of our Service.',
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 16,
                    color: Color(0xff889637),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  '3. Country refers to: South Africa.',
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 16,
                    color: Color(0xff889637),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  '4. Company (referred to as either “the Company”, “We”, “Us” or “Our” in this Agreement) refers to The Nail Deck.',
                  style: TextStyle(
                    fontFamily: "roboto",
                    fontSize: 16,
                    color: Color(0xff889637),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
