import 'package:flutter/material.dart';
import 'package:m3_assessment_1/screens/chat.dart';
import 'package:m3_assessment_1/screens/privacy.dart';
import 'package:m3_assessment_1/screens/profile.dart';
import 'package:m3_assessment_1/screens/terms.dart';
import 'package:m3_assessment_1/themes/palette.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<_MyHomePageState> user = GlobalKey<_MyHomePageState>();
  String get username => _username;

  String _username = "";

  set username(String value) {
    setState(() {
      _username = value;
    });
  }

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      body: Scaffold(
        backgroundColor: const Color(0xff889637),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(
                height: 40,
              ),
              Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(75.0),
                    topRight: Radius.circular(75.0),
                  ),
                  color: Color(0xfff7f7f7),
                ),
                height: 705,
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: Center(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Welcome ${user.currentState?.username}",
                        style: const TextStyle(
                            color: Palette.kToDark,
                            fontFamily: "roboto",
                            fontSize: 34,
                            fontWeight: FontWeight.bold),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 30.0),
                        child: Image(
                          image: AssetImage("assets/naildeck.jpeg"),
                          width: 200.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 60.0),
                        child: Column(children: [
                          const SizedBox(
                            height: 100.0,
                          ),
                          InkWell(
                            onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) => const Profile())),
                            child: Container(
                              height: 60.0,
                              width: 250,
                              decoration: BoxDecoration(
                                  color: Colors.blueGrey,
                                  borderRadius: BorderRadius.circular(20.0)),
                              child: const Center(
                                child: Text(
                                  "Edit your profile",
                                  style: TextStyle(
                                      fontFamily: "borooto",
                                      fontSize: 20.0,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 30.0,
                          ),
                          Row(
                            children: [
                              const SizedBox(
                                height: 100.0,
                              ),
                              ElevatedButton(
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const TermsOfUse())),
                                  child: const Text("T's and C's")),
                              const SizedBox(
                                width: 20.0,
                              ),
                              ElevatedButton(
                                  onPressed: () => Navigator.of(context).push(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const Privacy())),
                                  child: const Text("Privacy Notice"))
                            ],
                          ),
                        ]),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => Chat(),
          ),
        ),
        tooltip: 'Chat',
        child: const Icon(Icons.chat_bubble_outline_rounded),
      ),
    );
  }
}

class Service extends StatelessWidget {
  const Service({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Row(
        children: [],
      ),
    );
  }
}
