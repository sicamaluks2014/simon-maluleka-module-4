import 'package:flutter/material.dart';
import 'package:m3_assessment_1/screens/registration.dart';
// import 'package:url_launcher/url_launcher.dart';

import 'home.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool checked = false;
  final termsUrl = "https://";
  final privacyUrl = "https://";

  bool _toggleChecked() {
    return checked = !checked;
  }

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
  }

  // void _launchUrl(Uri url) async {
  //   if (!await launchUrl(url)) throw 'Could not launch $url';
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color(0xff889637),
      body: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
            height: 150,
            width: MediaQuery.of(context).size.width,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(75.0),
                bottomRight: Radius.circular(75.0),
              ),
              color: Color(0xfff7f7f7),
            ),
            child: const Center(
              child: Image(
                image: AssetImage('assets/naildeck.jpeg'),
              ),
            ),
          ),
          const SizedBox(
            height: 20.0,
          ),
          Center(
            child: Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(75.0),
                    topRight: Radius.circular(75.0),
                  ),
                  color: Color(0xfff7f7f7),
                ),
                height: 632,
                padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'Welcome back',
                      style: TextStyle(
                          color: Color(0xff889637),
                          fontFamily: 'roboto',
                          letterSpacing: 5.0,
                          fontWeight: FontWeight.bold,
                          fontSize: 34),
                    ),
                    const SizedBox(
                      height: 100,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Column(
                        children: [
                          TextFormField(
                            decoration: const InputDecoration(
                              hintText: 'E-mail',
                            ),
                          ),
                          TextFormField(
                            decoration: const InputDecoration(
                              hintText: 'Password',
                            ),
                            obscureText: true,
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          children: [
                            Checkbox(
                                value: checked,
                                activeColor: const Color(0xff889637),
                                onChanged: (v) {
                                  setState(() {
                                    _toggleChecked();
                                  });
                                }),
                            const Text("Remember me "),
                          ],
                        ),
                        RichText(
                          text: const TextSpan(
                            text: "Forgot password?",
                            style: TextStyle(
                                color: Color(0xff889637),
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "Sign In",
                            style: TextStyle(
                              fontSize: 30.0,
                              color: Color(0xff889637),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          FloatingActionButton.large(
                            onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => const MyHomePage(
                                  title: "The Nail Deck",
                                ),
                              ),
                            ),
                            foregroundColor: const Color(0xfff7f7f7),
                            elevation: 10.0,
                            tooltip: "Sign-in",
                            backgroundColor: const Color(0xff889637),
                            child: const Icon(Icons.arrow_forward),
                          ),
                        ],
                      ),
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: InkWell(
                          onTap: (() => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) => const Register()))),
                          child: RichText(
                            text: const TextSpan(
                              text: "Sign Up",
                              style: TextStyle(
                                  color: Color(0xff889637),
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
                    ]),
                    const SizedBox(height: 130.0),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Text(
                            "App developed by ",
                            style: TextStyle(
                              color: Colors.lightBlue,
                            ),
                          ),
                          Text(
                            "Simon Maluleka",
                            style: TextStyle(
                                color: Color(0xff889637),
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ])
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
