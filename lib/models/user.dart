class User {
  final String name;
  final String surname;
  final String phone;
  User(
    this.name,
    this.surname,
    this.phone,
  );
}
