import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:m3_assessment_1/screens/login.dart';
import 'package:m3_assessment_1/themes/palette.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: [
    SystemUiOverlay.bottom,
  ]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'M4-Assessment-1',
        theme: ThemeData(
            primarySwatch: Palette.kToDark,
            fontFamily: 'roboto',
            textTheme: const TextTheme(
                headline1:
                    TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
                headline6:
                    TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
                bodyText2: TextStyle(fontSize: 14.0))),
        home: AnimatedSplashScreen(
            splashTransition: SplashTransition.rotationTransition,
            duration: 3000,
            splash: const Scaffold(
              backgroundColor: Palette.kToDark,
              body: Center(
                child: Text(
                  "The App Deck",
                  style: TextStyle(
                      fontFamily: '',
                      fontSize: 42,
                      fontWeight: FontWeight.bold,
                      color: Color.fromARGB(255, 12, 29, 37)),
                ),
              ),
            ),
            nextScreen: const Login()),
      ),
    );
  }
}
